package com.project.farjad.player.buylistproject.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.project.farjad.player.buylistproject.R;

public class AlertDialogDelete extends DialogFragment {
    private Button btn_yes;
    private Button btn_no;
    private OnYesDeleteAllItemsClicked onYesDeleteAllItemsClicked;

    public AlertDialogDelete(OnYesDeleteAllItemsClicked onYesDeleteAllItemsClicked) {
        this.onYesDeleteAllItemsClicked = onYesDeleteAllItemsClicked;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_alert_layout, null, false);
        dialog.setView(view);
        dialog.setCancelable(false);


        btn_no = view.findViewById(R.id.btn_no_exit);
        btn_yes = view.findViewById(R.id.btn_yes_delete);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onYesDeleteAllItemsClicked.OnClickYes();
                dismiss();
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return dialog.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public interface OnYesDeleteAllItemsClicked {
        void OnClickYes();
    }
}
