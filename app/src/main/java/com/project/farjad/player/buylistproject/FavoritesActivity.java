package com.project.farjad.player.buylistproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.idescout.sql.SqlScoutServer;
import com.project.farjad.player.buylistproject.Database.SqlitePurchaseOpenHelper;
import com.project.farjad.player.buylistproject.Dialogs.EditBottomSheetDialog;
import com.project.farjad.player.buylistproject.Dialogs.PurchaseBottomSheetDialog;
import com.project.farjad.player.buylistproject.Model.Purchases;

import me.toptas.fancyshowcase.FancyShowCaseView;

public class FavoritesActivity extends AppCompatActivity implements PurchaseFavoritesAdapter.OnFavoritePurchaseListener, View.OnClickListener, EditBottomSheetDialog.EditPurchaseListener {
    private Toolbar toolbar;
    private RecyclerView rcl_fav;
    private PurchaseFavoritesAdapter adapter;
    private SqlitePurchaseOpenHelper database;
    private FloatingActionButton btn_add_fav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        SqlScoutServer.create(this, getPackageName());
        InitViews();
        adapter = new PurchaseFavoritesAdapter(this, database.getFavoritesPurchase(), this);
        rcl_fav.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rcl_fav.setAdapter(adapter);

        SharedPreferences setting=getSharedPreferences("intro",MODE_PRIVATE);
        if (setting.getBoolean("my_first_time",true)){
            SetupShowCaseView();
            setting.edit().putBoolean("my_first_time",false).apply();
        }

    }

    private void InitViews() {
        database = new SqlitePurchaseOpenHelper(this);
        toolbar = findViewById(R.id.toolbar_fav);
        rcl_fav = findViewById(R.id.rcl_fav);
        btn_add_fav = findViewById(R.id.btn_add_fav);
        btn_add_fav.setOnClickListener(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnDeleteFav(Purchases purchases) {
        int result = database.deletePurchaseFav(purchases);
        if (result > 0) {
            adapter.deletePurchaseFav(purchases);
        }

    }

    @Override
    public void OnClickFav(Purchases purchases) {
        Intent intent = new Intent();
        intent.putExtra("key_pur", purchases);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void OnFavoriteLongPressed(Purchases purchases) {
        EditBottomSheetDialog dialog = new EditBottomSheetDialog(false);
        Bundle bundle = new Bundle();
        bundle.putParcelable("purchase", purchases);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_fav: {
                PurchaseBottomSheetDialog dialog = new PurchaseBottomSheetDialog(new PurchaseBottomSheetDialog.addnewPurchaseListener() {
                    @Override
                    public void addNewPurchase(Purchases purchases) {
                        long id_task = database.addPurchaseFavorite(purchases);
                        if (id_task > -1) {
                            purchases.setId((int) id_task);
                            adapter.addPurchaseFav(purchases);
                            rcl_fav.smoothScrollToPosition(0);
                        }

                    }
                });
                dialog.show(getSupportFragmentManager(), null);
            }
            break;
        }
    }

    @Override
    public void OnEditPurchase(Purchases purchase) {
        database.updatePurchaseFav(purchase);
        adapter.updateItemPurchaseFav(purchase);
    }

    @Override
    public void OnAddToFavorite(Purchases purchases) {

    }

    public void SetupShowCaseView(){
        new FancyShowCaseView.Builder(this)
                .title("در اینجا می توانید کالا های منتخب خود را وارد کنید سپس تنها با کلیک بر روی آن به لیست اصلی اضافه خواهد شد")
                .titleSize(50,0)
                .titleGravity(Gravity.CENTER)
                .build()
                .show();

    }
}


