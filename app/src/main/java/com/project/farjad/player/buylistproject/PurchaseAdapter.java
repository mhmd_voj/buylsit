package com.project.farjad.player.buylistproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.project.farjad.player.buylistproject.Model.Purchases;

import java.util.List;

public class PurchaseAdapter extends RecyclerView.Adapter<PurchaseAdapter.PurchaseViewHolder> {

    private Context context;
    private List<Purchases> purchases;
    private OnItemClickPurchaseListener onItemClickPurchaseListener;

    public PurchaseAdapter(Context context, List<Purchases> purchases, OnItemClickPurchaseListener onItemClickPurchaseListener) {
        this.context = context;
        this.purchases = purchases;
        this.onItemClickPurchaseListener = onItemClickPurchaseListener;
    }

    @NonNull
    @Override
    public PurchaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PurchaseViewHolder(LayoutInflater.from(context).inflate(R.layout.purchase_item_list, parent, false));
    }

    public void addPurchase(Purchases purchase) {
        purchases.add(purchases.size(), purchase);
        notifyItemInserted(purchases.size());
    }

    @Override
    public void onBindViewHolder(@NonNull PurchaseViewHolder holder, int position) {
        holder.bindData(purchases.get(position));
    }

    void addPurchaseList(List<Purchases> purchases) {
        this.purchases.addAll(purchases);
        notifyDataSetChanged();

    }

    void SetChangeSearch(List<Purchases> purchases_search) {
        this.purchases = purchases_search;
        notifyDataSetChanged();
    }

    void ClearList() {
        this.purchases.clear();
        notifyDataSetChanged();
    }

    void deletePurchase(Purchases purchase) {
        for (int i = 0; i < purchases.size(); i++) {
            if (purchases.get(i).getId() == purchase.getId()) {
                purchases.remove(purchase);
                notifyItemRemoved(i);
                break;
            }

        }
    }

    @Override
    public int getItemCount() {
        return purchases.size();
    }

    public void updateItemPurchase(Purchases purchase) {
        for (int i = 0; i < purchases.size(); i++) {
            if (purchases.get(i).getId() == purchase.getId()) {
                purchases.set(i, purchase);
                notifyItemChanged(i);
                break;
            }

        }

    }

    class PurchaseViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_cost;
        private CheckBox chk_purchase;
        private ImageView img_improtance;
        private ImageView btn_remove;
        private TextView txt_amount;

        public PurchaseViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_cost = itemView.findViewById(R.id.txt_cost);
            chk_purchase = itemView.findViewById(R.id.chk_purchase);
            txt_amount = itemView.findViewById(R.id.txt_amount);
            img_improtance = itemView.findViewById(R.id.img_importance);
            btn_remove = itemView.findViewById(R.id.btn_remove_item);

        }

        void bindData(final Purchases purchases) {
            chk_purchase.setOnCheckedChangeListener(null);
            chk_purchase.setChecked(purchases.isBought());
            txt_cost.setText((purchases.getCost()) + " تومان");
            txt_amount.setText(purchases.getAmount());
            chk_purchase.setText(purchases.getName());
            if (purchases.getImportance().equalsIgnoreCase("مهم")) {
                img_improtance.setVisibility(View.VISIBLE);
            } else {
                img_improtance.setVisibility(View.INVISIBLE);
            }
            btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickPurchaseListener.OnDelete(purchases);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    onItemClickPurchaseListener.OnItemLongPressed(purchases);
                    return true;
                }
            });

            chk_purchase.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    purchases.setBought(b);
                    onItemClickPurchaseListener.OnCheckBought(purchases);
                }
            });
        }
    }

    interface OnItemClickPurchaseListener {
        void OnDelete(Purchases purchases);

        void OnItemLongPressed(Purchases purchases);

        void OnCheckBought(Purchases purchases);
    }
}
