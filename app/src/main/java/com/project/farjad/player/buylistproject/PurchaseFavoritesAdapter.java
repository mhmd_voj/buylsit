package com.project.farjad.player.buylistproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.project.farjad.player.buylistproject.Model.Purchases;

import java.util.List;

public class PurchaseFavoritesAdapter extends RecyclerView.Adapter<PurchaseFavoritesAdapter.FavoritesPurchaseViewHolder> {


    private Context context;
    private List<Purchases> purchases;
    private OnFavoritePurchaseListener onFavoritePurchaseListener;


    public PurchaseFavoritesAdapter(Context context, List<Purchases> purchases, OnFavoritePurchaseListener onFavoritePurchaseListener) {
        this.context = context;
        this.purchases = purchases;
        this.onFavoritePurchaseListener = onFavoritePurchaseListener;
    }


    @NonNull
    @Override
    public FavoritesPurchaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FavoritesPurchaseViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_fav_item_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FavoritesPurchaseViewHolder holder, int position) {
        holder.bindFavData(purchases.get(position));
    }

    public void addPurchaseFav(Purchases purchase) {
        purchases.add(purchases.size(), purchase);
        notifyItemInserted(purchases.size());
    }

    public void updateItemPurchaseFav(Purchases purchase) {
        for (int i = 0; i < purchases.size(); i++) {
            if (purchases.get(i).getId() == purchase.getId()) {
                purchases.set(i, purchase);
                notifyItemChanged(i);
                break;
            }

        }

    }

    void deletePurchaseFav(Purchases purchase) {
        for (int i = 0; i < purchases.size(); i++) {
            if (purchases.get(i).getId() == purchase.getId()) {
                purchases.remove(purchase);
                notifyItemRemoved(i);
                break;
            }

        }
    }

    @Override
    public int getItemCount() {
        return purchases.size();
    }

    public class FavoritesPurchaseViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_cost_fav;
        private TextView txt_purchase_fav;
        private ImageView img_improtance_fav;
        private ImageView btn_remove_fav;
        private TextView txt_amount_fav;

        public FavoritesPurchaseViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_purchase_fav = itemView.findViewById(R.id.txt_purchase_fav);
            txt_cost_fav = itemView.findViewById(R.id.txt_cost_fav);
            txt_amount_fav = itemView.findViewById(R.id.txt_amount_fav);
            img_improtance_fav = itemView.findViewById(R.id.img_importance_fav);
            btn_remove_fav = itemView.findViewById(R.id.btn_remove_item_fav);
        }

        public void bindFavData(final Purchases purchases) {
            txt_cost_fav.setText((purchases.getCost()) + " تومان");
            txt_amount_fav.setText(purchases.getAmount());
            txt_purchase_fav.setText(purchases.getName());
            if (purchases.getImportance().equalsIgnoreCase("مهم")) {
                img_improtance_fav.setVisibility(View.VISIBLE);
            } else {
                img_improtance_fav.setVisibility(View.INVISIBLE);
            }
            btn_remove_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onFavoritePurchaseListener.OnDeleteFav(purchases);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onFavoritePurchaseListener.OnClickFav(purchases);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    onFavoritePurchaseListener.OnFavoriteLongPressed(purchases);
                    return true;
                }
            });
        }
    }

    public interface OnFavoritePurchaseListener {
        void OnDeleteFav(Purchases purchases);

        void OnClickFav(Purchases purchases);

        void OnFavoriteLongPressed(Purchases purchases);
    }
}
