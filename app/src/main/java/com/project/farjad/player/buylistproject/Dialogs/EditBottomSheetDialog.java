package com.project.farjad.player.buylistproject.Dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.project.farjad.player.buylistproject.Database.SqlitePurchaseOpenHelper;
import com.project.farjad.player.buylistproject.Model.Purchases;
import com.project.farjad.player.buylistproject.R;

public class EditBottomSheetDialog extends BottomSheetDialogFragment implements View.OnClickListener {
    private EditText edt_name_purchase;
    private EditText edt_cost;
    private RadioGroup rdg_importance;
    private EditText edt_amount_purchase;
    private Button btn_add_to_fav;

    private Button btn_done;
    private Button btn_cancel;
    private Purchases purchases;

    private String Importance_buy_edit = "";

    private EditPurchaseListener editPurchaseListener;
    private boolean inMain;

    public EditBottomSheetDialog(boolean inMain) {
        this.inMain = inMain;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        editPurchaseListener = (EditPurchaseListener) context;
        purchases = getArguments().getParcelable("purchase");
        if (purchases == null) {
            dismiss();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.botton_sheet_dialog_edit_layout, null, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        edt_cost = view.findViewById(R.id.edt_cost_edit);
        btn_add_to_fav=view.findViewById(R.id.btn_add_to_fav);
        edt_amount_purchase=view.findViewById(R.id.edt_amount_purchase_edit);
        edt_name_purchase = view.findViewById(R.id.edt_new_purchase_edit);
        rdg_importance = view.findViewById(R.id.rdg_importance_edit);
        btn_cancel = view.findViewById(R.id.btn_cancel_edit);
        btn_done = view.findViewById(R.id.btn_done_edit);
        edt_name_purchase.setText(purchases.getName());
        edt_cost.setText(String.valueOf(purchases.getCost()));
        edt_amount_purchase.setText(purchases.getAmount());
        if (inMain){
            btn_add_to_fav.setVisibility(View.VISIBLE);
        }

        btn_done.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        btn_add_to_fav.setOnClickListener(this);
        Importance_buy_edit = purchases.getImportance();

        if (purchases.getImportance().equalsIgnoreCase("مهم")) {
            rdg_importance.check(R.id.rdb_high_importance_edit);
        } else if (purchases.getImportance().equalsIgnoreCase("کم")) {
            rdg_importance.check(R.id.rdb_low_importance_edit);
        }
        rdg_importance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rdb_high_importance_edit:
                        Importance_buy_edit = "مهم";
                        break;
                    case R.id.rdb_low_importance_edit:
                        Importance_buy_edit = "کم";
                        break;
                }
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel_edit: {
                dismiss();
            }
            break;
            case R.id.btn_done_edit: {
                if (edt_name_purchase.length() > 0 ) {
                    purchases.setName(edt_name_purchase.getText().toString());
                    purchases.setAmount(edt_amount_purchase.getText().toString());
                    purchases.setCost(Integer.valueOf(edt_cost.getText().toString()));
                    purchases.setImportance(Importance_buy_edit);
                    editPurchaseListener.OnEditPurchase(purchases);
                    dismiss();
                } else {
                    edt_name_purchase.setError("نام کالا را وارد کنید");
                }
            }
            break;
            case R.id.btn_add_to_fav:{
                editPurchaseListener.OnAddToFavorite(purchases);
                dismiss();
            }break;
        }
    }

    public interface EditPurchaseListener {
        void OnEditPurchase(Purchases purchase);
        void OnAddToFavorite(Purchases purchases);
    }
}
