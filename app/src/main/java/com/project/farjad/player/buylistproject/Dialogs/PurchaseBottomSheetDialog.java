package com.project.farjad.player.buylistproject.Dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.project.farjad.player.buylistproject.Model.Purchases;
import com.project.farjad.player.buylistproject.R;

public class PurchaseBottomSheetDialog extends BottomSheetDialogFragment implements View.OnClickListener {
    private EditText edt_name_purchase;
    private EditText edt_cost;
    private RadioGroup rdg_importance;
    private Button btn_done;
    private Button btn_cancel;
    private EditText edt_amount_purchase;
    private String Importance_buy = "کم";
    private addnewPurchaseListener callback;


    public PurchaseBottomSheetDialog(addnewPurchaseListener callback) {
        this.callback = callback;

    }

    public PurchaseBottomSheetDialog() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.botton_sheet_dialog_layout, null, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        edt_cost = view.findViewById(R.id.edt_cost);
        edt_name_purchase = view.findViewById(R.id.edt_new_purchase);
        rdg_importance = view.findViewById(R.id.rdg_importance);
        btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_done = view.findViewById(R.id.btn_done);
        edt_amount_purchase = view.findViewById(R.id.edt_amount_purchase);
        btn_done.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        rdg_importance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rdb_high_importance:
                        Importance_buy = "مهم";
                        break;
                    case R.id.rdb_low_importance:
                        Importance_buy = "کم";
                        break;
                }
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel: {
                dismiss();
            }
            break;
            case R.id.btn_done: {
                if (edt_name_purchase.length() > 0) {

                    Purchases purchases = new Purchases();
                    purchases.setName(edt_name_purchase.getText().toString());
                    if (edt_amount_purchase.length() == 0) {
                        purchases.setAmount("بدون مقدار");
                        Toast.makeText(getActivity(), "مقدار کالا را وارد نکردید", Toast.LENGTH_SHORT).show();
                    } else {
                        purchases.setAmount(edt_amount_purchase.getText().toString());
                    }
                    if (edt_cost.length() == 0) {
                        purchases.setCost(0);
                        Toast.makeText(getActivity(), "قیمت کالا را وارد نکردید", Toast.LENGTH_SHORT).show();
                    } else {
                        purchases.setCost(Integer.parseInt(edt_cost.getText().toString()));
                    }
                    purchases.setBought(false);
                    purchases.setImportance(Importance_buy);
                    callback.addNewPurchase(purchases);
                    dismiss();


                } else {
                    edt_name_purchase.setError("نام کالا را وارد کنید");
                }
            }
            break;
        }
    }

    public interface addnewPurchaseListener {
        public void addNewPurchase(Purchases purchases);
    }


}
