package com.project.farjad.player.buylistproject.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Purchases implements Parcelable {
    private int id;
    private String name;
    private String amount;
    private int cost;
    private boolean isBought;
    private String Importance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public boolean isBought() {
        return isBought;
    }

    public void setBought(boolean bought) {
        isBought = bought;
    }

    public String getImportance() {
        return Importance;
    }
    public void setImportance(String importance) {
        Importance = importance;
    }


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.amount);
        dest.writeInt(this.cost);
        dest.writeByte(this.isBought ? (byte) 1 : (byte) 0);
        dest.writeString(this.Importance);
    }

    public Purchases() {
    }

    protected Purchases(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.amount = in.readString();
        this.cost = in.readInt();
        this.isBought = in.readByte() != 0;
        this.Importance = in.readString();
    }

    public static final Parcelable.Creator<Purchases> CREATOR = new Parcelable.Creator<Purchases>() {
        @Override
        public Purchases createFromParcel(Parcel source) {
            return new Purchases(source);
        }

        @Override
        public Purchases[] newArray(int size) {
            return new Purchases[size];
        }
    };
}
