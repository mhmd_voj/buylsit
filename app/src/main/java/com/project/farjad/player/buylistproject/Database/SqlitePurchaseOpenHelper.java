package com.project.farjad.player.buylistproject.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.project.farjad.player.buylistproject.Model.Purchases;

import java.util.ArrayList;
import java.util.List;

public class SqlitePurchaseOpenHelper extends SQLiteOpenHelper {

    static final public String TABLE_NAME = "purchases_table";
    static final public String TABLE_FAV_NAME = "favorite_purchases_table";

    public SqlitePurchaseOpenHelper(@Nullable Context context) {
        super(context, "purchase_db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_NAME + " (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT,amount TEXT, cost INTEGER, bought BOOLEAN, importance TEXT);");
            sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_FAV_NAME + "(id INTEGER PRIMARY KEY AUTOINCREMENT, name_fav TEXT,amount_fav TEXT,cost_fav INTEGER,bought_fav BOOLEAN,importance_fav TEXT);");
        } catch (SQLException e) {
            Log.i("Create table Exception", "onCreate: " + e);
        }


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


    public long addPurchase(Purchases purchases, String table_name) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", purchases.getName());
        contentValues.put("amount", purchases.getAmount());
        contentValues.put("cost", purchases.getCost());
        contentValues.put("bought", purchases.isBought());
        contentValues.put("importance", purchases.getImportance());
        long result = database.insert(table_name, null, contentValues);
        database.close();
        return result;

    }

    public List<Purchases> getPurchase() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        List<Purchases> purchases = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Purchases purchase = new Purchases();
                purchase.setId(cursor.getInt(0));
                purchase.setName(cursor.getString(1));
                purchase.setAmount(cursor.getString(2));
                purchase.setCost(cursor.getInt(3));
                purchase.setBought(cursor.getInt(4) == 1);
                purchase.setImportance(cursor.getString(5));
                purchases.add(purchase);
            } while (cursor.moveToNext());
        }

        sqLiteDatabase.close();
        return purchases;
    }

    public void updatePurchase(Purchases purchases) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", purchases.getName());
        contentValues.put("amount", purchases.getAmount());
        contentValues.put("cost", purchases.getCost());
        contentValues.put("bought", purchases.isBought());
        contentValues.put("importance", purchases.getImportance());
        database.update(TABLE_NAME, contentValues, "id = ?", new String[]{String.valueOf(purchases.getId())});
        database.close();


    }

    public int deletePurchase(Purchases purchases) {
        SQLiteDatabase database = getWritableDatabase();
        int result = database.delete(TABLE_NAME, "id = ?", new String[]{String.valueOf(purchases.getId())});
        database.close();
        return result;
    }

    public void clearAllPurchases() {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(" DELETE FROM " + TABLE_NAME);
    }

    public List<Purchases> searchInDatabase(String text) {
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE name LIKE '%" + text + "%'", null);
        List<Purchases> purchases = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Purchases purchase = new Purchases();
                purchase.setId(cursor.getInt(0));
                purchase.setName(cursor.getString(1));
                purchase.setAmount(cursor.getString(2));
                purchase.setCost(cursor.getInt(3));
                purchase.setBought(cursor.getInt(4) == 1);
                purchase.setImportance(cursor.getString(5));
                purchases.add(purchase);
            } while (cursor.moveToNext());
        }

        database.close();
        return purchases;

    }

    public List<Purchases> getFavoritesPurchase() {
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_FAV_NAME, null);
        List<Purchases> purchases_fav = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Purchases purchase = new Purchases();
                purchase.setId(cursor.getInt(0));
                purchase.setName(cursor.getString(1));
                purchase.setAmount(cursor.getString(2));
                purchase.setCost(cursor.getInt(3));
                purchase.setBought(cursor.getInt(4) == 1);
                purchase.setImportance(cursor.getString(5));
                purchases_fav.add(purchase);

            } while (cursor.moveToNext());
        }
        return purchases_fav;
    }

    public long addPurchaseFavorite(Purchases purchases) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name_fav", purchases.getName());
        contentValues.put("amount_fav", purchases.getAmount());
        contentValues.put("cost_fav", purchases.getCost());
        contentValues.put("bought_fav", purchases.isBought());
        contentValues.put("importance_fav", purchases.getImportance());
        long result = database.insert(TABLE_FAV_NAME, null, contentValues);
        database.close();
        return result;

    }

    public int deletePurchaseFav(Purchases purchases) {
        SQLiteDatabase database = getWritableDatabase();
        int result = database.delete(TABLE_FAV_NAME, "id = ?", new String[]{String.valueOf(purchases.getId())});
        database.close();
        return result;
    }

    public void updatePurchaseFav(Purchases purchases) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name_fav", purchases.getName());
        contentValues.put("amount_fav", purchases.getAmount());
        contentValues.put("cost_fav", purchases.getCost());
        contentValues.put("bought_fav", purchases.isBought());
        contentValues.put("importance_fav", purchases.getImportance());
        database.update(TABLE_FAV_NAME, contentValues, "id = ?", new String[]{String.valueOf(purchases.getId())});
        database.close();


    }

}
