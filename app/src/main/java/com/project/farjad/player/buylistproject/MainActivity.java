package com.project.farjad.player.buylistproject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.idescout.sql.SqlScoutServer;
import com.project.farjad.player.buylistproject.Database.SqlitePurchaseOpenHelper;
import com.project.farjad.player.buylistproject.Dialogs.AlertDialogDelete;
import com.project.farjad.player.buylistproject.Dialogs.EditBottomSheetDialog;
import com.project.farjad.player.buylistproject.Dialogs.PurchaseBottomSheetDialog;
import com.project.farjad.player.buylistproject.Model.Purchases;

import java.util.List;

import me.toptas.fancyshowcase.FancyShowCaseQueue;
import me.toptas.fancyshowcase.FancyShowCaseView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, EditBottomSheetDialog.EditPurchaseListener {
    private EditText edt_search;
    private FloatingActionButton btn_add;
    private ImageView btn_clear;
    private RecyclerView rcl_mian;
    private ImageView btn_navigation_drawer;
    private SqlitePurchaseOpenHelper database;
    private DrawerLayout drawer_layout_main;
    private PurchaseAdapter purchaseAdapter;
    private NavigationView navigation_view_main;
    private SharedPreferences setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SqlScoutServer.create(this, getPackageName());
        InitViews();
        setting = getSharedPreferences("intro", MODE_PRIVATE);


    }

    private void InitViews() {
        edt_search = findViewById(R.id.edt_search_main);
        btn_add = findViewById(R.id.btn_add);
        btn_clear = findViewById(R.id.btn_clear);
        drawer_layout_main = findViewById(R.id.drawer_layout_main);
        rcl_mian = findViewById(R.id.rcl_main);
        navigation_view_main = findViewById(R.id.navigation_view_main);
        btn_navigation_drawer = findViewById(R.id.btn_navigation_drawer);
        btn_clear.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        btn_navigation_drawer.setOnClickListener(this);


        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    List<Purchases> list = database.searchInDatabase(charSequence.toString());
                    purchaseAdapter.SetChangeSearch(list);
                } else {
                    List<Purchases> list = database.getPurchase();
                    purchaseAdapter.SetChangeSearch(list);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        database = new SqlitePurchaseOpenHelper(this);
        purchaseAdapter = new PurchaseAdapter(this, database.getPurchase(), new PurchaseAdapter.OnItemClickPurchaseListener() {
            @Override
            public void OnDelete(Purchases purchases) {
                int result = database.deletePurchase(purchases);
                if (result > 0) {
                    purchaseAdapter.deletePurchase(purchases);
                }
            }

            @Override
            public void OnItemLongPressed(Purchases purchases) {
                EditBottomSheetDialog dialog = new EditBottomSheetDialog(true);
                Bundle bundle = new Bundle();
                bundle.putParcelable("purchase", purchases);
                dialog.setArguments(bundle);
                dialog.show(getSupportFragmentManager(), null);

            }

            @Override
            public void OnCheckBought(Purchases purchases) {
                database.updatePurchase(purchases);
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rcl_mian.setLayoutManager(layoutManager);
        rcl_mian.setAdapter(purchaseAdapter);

        //List<Purchases> purchases=database.getPurchase();
        //purchaseAdapter.addPurchaseList(purchases);

        navigation_view_main.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.favorites_purchase_menu: {
                        Intent intent = new Intent(MainActivity.this, FavoritesActivity.class);
                        startActivityForResult(intent, 1001);
                        drawer_layout_main.closeDrawer(Gravity.RIGHT);
                    }
                    break;
                    case R.id.share_menu: {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, " با لیست خرید بای دیگه کارات یادت نمیره" +
                                "به هم پشنهاد میکنم.");
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                    }
                    break;
                    case R.id.about_this: {
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.drawer_layout_main, new FragmentAboutUs());
                        transaction.addToBackStack(null);
                        transaction.commit();
                        drawer_layout_main.closeDrawer(Gravity.RIGHT);
                    }
                    break;
                    case R.id.exit: {
                        onBackPressed();
                    }
                    break;
                }
                return true;
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add: {
                PurchaseBottomSheetDialog dialog = new PurchaseBottomSheetDialog(new PurchaseBottomSheetDialog.addnewPurchaseListener() {
                    @Override
                    public void addNewPurchase(Purchases purchase) {
                        long taskId = database.addPurchase(purchase, SqlitePurchaseOpenHelper.TABLE_NAME);
                        if (taskId != -1) {
                            purchase.setId((int) taskId);
                            purchaseAdapter.addPurchase(purchase);
                            rcl_mian.smoothScrollToPosition(0);
                            if (setting.getBoolean("my_first_time_main", true)) {
                                new FancyShowCaseView.Builder(MainActivity.this)
                                        .title("با نگه داشتن روی کالا می توانید آنرا ویرایش کنید")
                                        .titleSize(50, 0)
                                        .titleGravity(Gravity.CENTER)
                                        .build()
                                        .show();
                                setting.edit().putBoolean("my_first_time_main",false).apply();
                            }
                        }
                    }
                });
                dialog.show(getSupportFragmentManager(), null);
            }
            break;

            case R.id.btn_clear: {
                if (purchaseAdapter.getItemCount() == 0) {
                    Toast.makeText(this, "هیج خریدی وجود ندارد", Toast.LENGTH_SHORT).show();
                } else {
                    AlertDialogDelete alertDialogDelete = new AlertDialogDelete(new AlertDialogDelete.OnYesDeleteAllItemsClicked() {
                        @Override
                        public void OnClickYes() {
                            database.clearAllPurchases();
                            purchaseAdapter.ClearList();
                        }
                    });
                    alertDialogDelete.show(getSupportFragmentManager(), null);
                }
            }
            break;

            case R.id.btn_navigation_drawer: {
                if (drawer_layout_main.isDrawerOpen(Gravity.RIGHT)) {
                    drawer_layout_main.closeDrawer(Gravity.RIGHT);
                } else {
                    drawer_layout_main.openDrawer(Gravity.RIGHT);
                }


            }
            break;
        }

    }

    @Override
    public void OnEditPurchase(Purchases purchase) {
        database.updatePurchase(purchase);
        purchaseAdapter.updateItemPurchase(purchase);


    }

    @Override
    public void OnAddToFavorite(Purchases purchases) {
        database.addPurchaseFavorite(purchases);
        Toast.makeText(this, "کالا به لیست منتخب اضافه شد", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Purchases purchases;
        if (requestCode == 1001 && resultCode == RESULT_OK) {
            if (data != null) {
                purchases = data.getParcelableExtra("key_pur");
                long taskId = database.addPurchase(purchases, SqlitePurchaseOpenHelper.TABLE_NAME);
                if (taskId != -1) {
                    purchases.setId((int) taskId);
                    purchaseAdapter.addPurchase(purchases);
                    rcl_mian.smoothScrollToPosition(0);
                }

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
